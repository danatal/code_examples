<?php
class validator
{
	private $error_code;
	private $errors;
	private $valid_vals;
	private $max_limit;
	private $min_limit;
        private $max_str_len;
        protected $origin_file = "api validator.php";
	
	public function __construct()
	{
		$this->error_code = null;
		$this->errors = array (								
                                '101'=>'Missing parameter',
                                '102'=>'Invalid parameter: only digits are allowed',
                                '103'=>'Invalid parameter: use only alphabet characters, hyphens or spaces',
                                '104'=>'Invalid parameter: use only allowed values (%s)',
                                '105'=>'Api key is wrong',
                                '106'=>'Invalid parameter: value exceeds maximum limit (%s)',
                                '107'=>'Invalid parameter: value exceeds minimum limit (%s)',
                                '108'=>'Invalid unique id : The format should be two digital strings joined by a dot',
                                '109'=>'Wrong format of numeric list',
                                '110'=>'Invalid parameter: use only alphabet characters,punctuation characters,digits,hyphens or spaces',
                                '111'=>'Invalid parameter: use only alphabet characters,hyphens,spaces or digits',
                                '112'=>'Wrong format of pipes list',
                                '113'=>'Wrong date format, the date should be formated like YYYY-MM-DD HH:MM:SS',
                                '114'=>'The date supplied is not a future date',
                                '115'=>'The input format is invalid, it does not follow the syntax indicated in the api guide',
                                '116'=>'The input string exceeds maximum length allowed (%s)'
                           );                
	}
	
	public function value_exists($input_arr,$fieldname)
	{
		$result = true;
		if ( !isset($input_arr[$fieldname]) || empty($input_arr[$fieldname]) )
		{
			$result = false;
			$this->error_code = '101';
		}
		return ($result);
	}
	
	public function only_digits($input)
	{
		$input=trim($input);
		$res = true;
		if ( preg_match('#^\d+$#',$input,$matches) == 0)
		{
			$res= false;
			$this->error_code = '102';
		}	
		return($res);
	}
	
	public function numbers_list($input)
	{
		$res = true;
		if ( preg_match('#^\d+(,\d+)*$#',$input,$matches)==0 )
		{
			$res = false;
			$this->error_code = '109';
		}
		return($res);		
	}
	
	public function pipes_list($input)
	{
		$res = true;
		
		$parts = explode("|",$input);
		if ( empty($parts) )
		{
			$res = false;
			$this->error_code = '112';
		}
		else
		{
			$pattern = "/^[\s\da-zA-Z_\-\\x{0590}-\\x{05FF}]+$/u";
			foreach($parts as $part)
			{
				 list($key,$val) = explode("=",$part);
				 if ( empty($key) || empty ($val) )
				 {
					$res = false;
					$this->error_code = '112'; 
				 }
				 if ( !preg_match($pattern,$key,$matches) || !preg_match($pattern,$val,$matches)  )
				{
					$res = false;
					$this->error_code = '112';
				}
			}
		}
		return($res);	
	}
	
	public function mysql_date($input)
	{
		$res = true;
		if ( preg_match('#^\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}$#',$input,$matches) == 0)
		{
			$res = false;
			$this->error_code = '113';
		}
		return($res);
	}
	
	public function future_mysql_date($input)
	{
		$res = $this->mysql_date($input);
		if ($res == true ) // if the date is syntactically good, check its in the future
		{
			$now = time();
			$time_of_date = strtotime($input);
			if ( $now >= $time_of_date )
			{
				$res = false;
				$this->error_code = '114';
			}
		}
		return ($res);
	}
	
	public function is_lower_or_equal($num,$max_limit)
	{
            $res = true;
            if ( $num > $max_limit )
            {
                    $res = false;
                    $this->max_limit = $max_limit;
                    $this->error_code = '106';
            }
            return $res;
	} 
        
        public function string_len($input_str,$max_len)
        {
            $res = true;
            $num = strlen($input_str);
            logger::writeLog("customer_id length=".$num." allowed=".$max_len, "callcenter.log",$this->origin_file);
            if ( $num > $max_len)
            {
                $res = false;
                $this->max_str_len = $max_len;
                $this->error_code = '116';
            }
            return $res;
        }
	
	public function is_bigger_or_equal($num,$min_limit)
	{
		$res = true;
		if ( $num < $min_limit )
		{
			$res = false;
			$this->min_limit = $min_limit;
			$this->error_code = '107';
		}
		return $res;
	}
	
	public function valid_uid($uid)
	{
		$res = true;
	
		if ( preg_match("#^\d+\.\d+$#",$uid,$matches)==0)
		{
			$res = false;
			$this->error_code = '108';
		}
		return $res;
	}

	public function valid_values($input,$valid_vals)
	{
		$res = true;
		$this->valid_vals = implode(",",$valid_vals);
		if ( ! in_array($input,$valid_vals) )
		{
			$res = false;
			$this->error_code = '104';
		}
		return $res;
	}
	
	public function valid_api_key($input)
	{
		global $config;
		$res = true;
				
                $api_key = $config['api_key'];
		if ( $input != $api_key )
		{
			$res = false;
			$this->error_code = '105';
		}
		 
	    return($res);
	}
	
	public function valid_name($name)
	{
		$res = true;
		
		// we allow english characters, hebrew characters , spaces, hyphens 
		
	   if ( ! preg_match("/^[-\d\sa-zA-Z\\x{0590}-\\x{05FF}]+$/u",$name,$matches)  )  // || preg_match("/[\\x{0590}-\\x{05FF}]+/u", $name,$matches) 
		{
			$res = false;
			$this->error_code = '103';
		}
		
		return $res;
	}
	
	public function valid_name_digits($name)
	{
		$res = true;
		
		// we allow english characters, hebrew characters , spaces, hyphens 
		
	   if ( ! preg_match("/^[-\sa-zA-Z\d\\x{0590}-\\x{05FF}]+$/u",$name,$matches)  )  // || preg_match("/[\\x{0590}-\\x{05FF}]+/u", $name,$matches) 
		{
			$res = false;
			$this->error_code = '111';
		}
		
		return $res;
	}
	
	public function valid_content($input)
	{
		$res = true;
		
		if ( ! preg_match("/^[?:!,\.-\s\da-zA-Z\\x{0590}-\\x{05FF}]+$/u",$input,$matches)  )  
		{
			$res = false;
			$this->error_code = '110';
		}		
		return $res;
	}
	
	
	public function valid_pattern($input,$pattern)
	{
		$res = true;
		if (! preg_match_all($pattern,$input,$matches) )
		{
			$res=false;
			$this->error_code = '115';
		}
		return($res);
	}
	
	public function get_error_info()
	{
		$info = array();
		$info['code'] = $this->error_code;
		
		switch($this->error_code)
		{
			case '104':
				       $this->errors[$this->error_code] = sprintf($this->errors[$this->error_code],$this->valid_vals);
				       break;
			case '106':
				       $this->errors[$this->error_code] = sprintf($this->errors[$this->error_code],$this->max_limit);
				       break;
			case '107':
				       $this->errors[$this->error_code] = sprintf($this->errors[$this->error_code],$this->min_limit);
				       break;
                        case '116':
                                       $this->errors[$this->error_code] = sprintf($this->errors[$this->error_code],$this->max_str_len);
                                       break;
		}
		
		$info['description'] = $this->errors[$this->error_code];
		return($info);
	}
	
	
}
?>