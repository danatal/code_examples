<?php

final class Database
{
   private $server;
   private $username;
   private $password;
   private $db_name;
   private $link;

   private static $instances = array();
   
   private function __construct($server,$username,$password,$db_name,$report_exceptions=false)
   {
      $this->server = $server;
      $this->username = $username;
      $this->password = $password;
      $this->db_name  = $db_name;
      $this->connect();
      if ($report_exceptions)
      {
        mysqli_report(MYSQLI_REPORT_ALL);
      }
   }
   
   public function __destruct() {
       $this->close();
   }

   private function connect()
   {
      $this->link = mysqli_connect($this->server,$this->username,$this->password,$this->db_name);
      mysqli_query($this->link, "SET NAMES 'utf8'");
      mysqli_query($this->link, "set character_set_client='utf8'");
      mysqli_query($this->link, "set character_set_results='utf8'");
      mysqli_query($this->link, "set collation_connection='utf8'");

      if (mysqli_connect_errno())
      {
        throw new Exception('Unable to establish database connection: ' .mysqli_connect_error().
                '\nDetails: Server'.$this->server.' User name-'.$this->username.' Password-'.$this->password.' DB name-'.$this->db_name);
      }
  
   }
   
   public function __clone()
   {
      triger_error("cloning is not allowed");
   }
   
   public static function get_instance($server,$username,$password,$db_name)
   {
      if ( ! isset(self::$instances[$db_name]) )
      {
         self::$instances[$db_name] = new Database($server,$username,$password,$db_name,$report_exceptions);
      }
   
      return(self::$instances[$db_name]);
   }
   
   
   
   public function query($query) 
   {        
        $result = mysqli_query($this->link,$query);
        if (!$result) {           
            throw new Exception(PHP_EOL." Query Error: " . mysqli_error($this->link).PHP_EOL."DB=".$this->db_name.PHP_EOL."Query=".$query);
        }
        return $result;
    }
    
    
    
    public function get_assoc_output($query)
    {
        $output = array();
        $result=$this->query($query);
        while( $row=mysqli_fetch_assoc($result))
        {
            $output[] = $row;
        }        
         mysqli_free_result($result);
        return($output);
    }
	
	
	public function get_index_output($query)
    {
        $output = array();
        $result=$this->query($query);
        while( $row=mysqli_fetch_array($result))
        {
            $output[] = $row;
        } 
         mysqli_free_result($result);
        return($output);
    }
	
	public function get_one_column_output($query)
	{
		$output = array();
        $result=$this->query($query);
        while( $row=mysqli_fetch_array($result))
        {
            $output[] = $row[0];
        }   
         mysqli_free_result($result);
        return($output);
	}
    
    public function free_result($result){ 
        mysqli_free_result($result);
    }

    
    public function close()
    {
            mysqli_close($this->link);
    }    
    
}

?>
