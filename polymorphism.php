<?php
abstract class Shape
{
	private $height;
	
	public function __construct($h)
	{
		$this->height = $h;
	}
	
	abstract protected function get_surface();
	
	public function get_volume()
	{
		return $this->height * $this->get_surface();
	}
}

class Cube extends Shape
{
	private $side;
	
	public function __construct($s,$h)
	{
		parent::__construct($h);
		$this->side = $s;
	}
	protected function get_surface()
	{
		return pow($this->side,2);
	}
}

class Cylinder extends Shape
{
	private $radius;
	const pai = 3.14;
	
	public function __construct($r,$h)
	{
		parent::__construct($h);
		$this->radius = $r;
	}
	
	protected function get_surface()
	{
		return (self::pai * pow($this->radius,2));
	}
	
	
}


$shapes = array( new Cube(3,4), new Cylinder(5,3) , new Cube(7,4) );
foreach ($shapes as $shape)
{
	$v = $shape->get_volume();
	echo "The volume of :<br>";
	var_dump($shape);
	echo "is :".$v."<p>";
	
}


?>